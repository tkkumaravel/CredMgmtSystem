<?php require_once("includes/session.php"); ?>
<?php require_once("includes/db_connection.php"); ?>
<html lang="en">
<head>
  <title>Credit management system</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<body background="backgr.jpg">

<div style="text-align:center" >
  <h1>Credit management system</h1>
</div>
<br/><br/>
<div style="text-align:center">
	    <a href="from_user.php">
        <button type="button" class="btn btn-outline-secondary">TRANSFER CREDITS</button>
        </a><br><br>
        <a href="view_user_list.php">
        <button type="button" class="btn btn-outline-secondary">VIEW USERS</button>
        </a>
</div>
</body>
</html>
<?php
//close connection.
mysqli_close($connection);
?>