-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 04, 2018 at 06:12 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `credit_transfer`
--

-- --------------------------------------------------------

--
-- Table structure for table `transfer`
--

CREATE TABLE `transfer` (
  `senderid` varchar(50) NOT NULL,
  `receiverid` varchar(50) NOT NULL,
  `credit` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transfer`
--

INSERT INTO `transfer` (`senderid`, `receiverid`, `credit`) VALUES
('A2', 'B0', 5),
('A2', 'B0', 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `unique_id` varchar(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` int(10) NOT NULL,
  `current_credit` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`unique_id`, `username`, `email`, `phone`, `current_credit`) VALUES
('A1', 'maa', 'amma@gmail.com', 12952, 60),
('A2', 'mary', 'gold@gmail.com', 4569, 65),
('A3', 'hari', 'hari@gmail.com', 34509, 15),
('A4', 'james', 'jam@gmail.com', 45679, 70),
('A5', 'john', 'jn@gmail.com', 1234, 40),
('A6', 'meghna', 'kattamuri.meghna@gmail.com', 5678, 50),
('A7', 'motu', 'moturi@gmail.com', 7809, 70),
('A8', 'papa', 'papa@gmail.com', 27809, 40),
('A9', 'sushi', 'sushi@gmail.com', 3459, 30),
('B0', 'teju', 'teja@gmail.com', 12809, 20);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`unique_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
